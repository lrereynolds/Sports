using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.XRay.Recorder.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace SendDailyEmailForHorseRacingPicks
{
    public class Function
    {
        
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string FunctionHandler(string input, ILambdaContext context)
        {
            LambdaLogger.Log("Get data" );
              //AWSXRayRecorder recorder = new AWSXRayRecorder();
              // The body of the email
              String BODY =
                  "<h1>Predictor</h1><table  border=1px> ";
              string sqlConnection = "Server=tcp:mustangsally.database.windows.net,1433;Initial Catalog=Sport;Persist Security Info=False;User ID=admin2;Password=tuggiw-2haffe-Fangav;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
              //recorder.BeginSubsegment("get data");
              //recorder.AddAnnotation("Getting", "predictor data");
              SqlConnection connection = new SqlConnection(sqlConnection);
              SqlDataReader rdr = null;
              SqlCommand cmd = new SqlCommand("HorseRacingPredictor", connection);

              // 2. set the command object so it knows
              //    to execute a stored procedure
              cmd.CommandType = CommandType.StoredProcedure;
            LambdaLogger.Log("Opening connection");
            cmd.Connection.Open();
            LambdaLogger.Log("Connection opened");
            // 3. add parameter to command, which
            //    will be passed to the stored procedure
            cmd.Parameters.Add(new SqlParameter("@Date", DateTime.UtcNow));

              // execute the command
              rdr = cmd.ExecuteReader();
            //recorder.BeginSubsegment("Data gotten");
            BODY = BODY + "<tr><td>count</td><td>finishcom</td><td>Finish</td><td>odds</td><td>race</td><td>course date</td><td>horse</td><td>HorseId</td><td>chance</td><td>distance</td><td>distanceCounts</td><td>going</td><td>goingCounts</td></tr>";
              while (rdr.Read())
                 {
                  BODY = BODY +"<tr><td>"+ rdr["count"]+"</td><td>"+   rdr["finishcom"] + "</td><td>" + rdr["Finish"] + "</td><td>" + rdr["odds"] + "</td><td>" + rdr["race couse"] + "</td><td>" + rdr["date"] + "</td><td>" + rdr["horse"] + "</td><td>" + rdr["HorseId"] + "</td><td>" + rdr["chance"] + "</td><td>" + rdr["distance"] + "</td><td>" + rdr["distanceCounts"] + "</td><td>" + rdr["going"] + "</td><td>" + rdr["goingCounts"]+ "</td></tr>";
                 }
              BODY = BODY + "</table>";
              rdr.Close();
              cmd.Dispose();
              //recorder.EndSubsegment();
              // Replace sender@example.com with your "From" address. 
              // This address must be verified with Amazon SES.
              String FROM = "lrereynolds@icloud.com";
              String FROMNAME = "Horse Race Predictions";

              // Replace recipient@example.com with a "To" address. If your account 
              // is still in the sandbox, this address must be verified.
              String TO = "lrereynolds@icloud.com";

              // Replace smtp_username with your Amazon SES SMTP user name.
              String SMTP_USERNAME = "AKIAYOF4EUOFVKNEMKV2";

              // Replace smtp_password with your Amazon SES SMTP password.
              String SMTP_PASSWORD = "BLlgN9/I9of0paeX/cWrbq0G8/Fev4UaKGcJjGHgNZbI";

              // (Optional) the name of a configuration set to use for this message.
              // If you comment out this line, you also need to remove or comment out

              // the "X-SES-CONFIGURATION-SET" header below.
              String CONFIGSET = "ConfigSet";

              // If you're using Amazon SES in a region other than US West (Oregon), 
              // replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP  
              // endpoint in the appropriate AWS Region.
              String HOST = "email-smtp.eu-west-1.amazonaws.com";

              // The port you will connect to on the Amazon SES SMTP endpoint. We
              // are choosing port 587 because we will use STARTTLS to encrypt
              // the connection.
              int PORT = 587;

              // The subject line of the email
              String SUBJECT =
                  "Horse Race Predictions";

              // Create and build a new MailMessage object
              MailMessage message = new MailMessage();
              message.IsBodyHtml = true;
              message.From = new MailAddress(FROM, FROMNAME);
              message.To.Add(new MailAddress(TO));
              message.Subject = SUBJECT;
              message.Body = BODY;
              // Comment or delete the next line if you are not using a configuration set
              //message.Headers.Add("X-SES-CONFIGURATION-SET", CONFIGSET);
              //recorder.BeginSubsegment("Send email");
            LambdaLogger.Log("Send email");
            using (var client = new System.Net.Mail.SmtpClient(HOST, PORT))
              {
                  // Pass SMTP credentials
                  client.Credentials =
                      new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);

                  // Enable SSL encryption
                  client.EnableSsl = true;

                  // Try to send the message. Show status in console.
                  try
                  {
                      Console.WriteLine("Attempting to send email...");
                      client.Send(message);
                      Console.WriteLine("Email sent!");
                  }
                  catch (Exception ex)
                  {
                //      recorder.AddAnnotation("Error","email not sent");
                                          Console.WriteLine("The email was not sent.");
                      Console.WriteLine("Error message: " + ex.Message);
                  }
              }
             // recorder.EndSubsegment();
            return input?.ToUpper();
        }
    }
}
