﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;

namespace DogRacing
{
    public class AddDogRace
    {
        public string AddNewRace(DateTime fromDate, DateTime toDate)
        {
            //DateTime fromDate = new DateTime(2022, 08, 07);
            //DateTime toDate = new DateTime(2022, 09, 28);

            for (DateTime day = fromDate; day.Date <= toDate; day = day.AddDays(1))
            {
                string url = "https://www.sportinglife.com/greyhounds/results/" + day.ToString("yyyy-MM-dd");
                var web = new HtmlAgilityPack.HtmlWeb();
                HtmlDocument doc = web.Load(url);
                var navigator = doc.CreateNavigator();

                List<HtmlNode> toftitle = doc.DocumentNode.Descendants().Where(x => (x.Name == "div" && x.Attributes["class"] != null)).ToList();
                //*[@id="container"]/section[2]/span[1]/div/section/ul/li[2]/section/div/div[1]/h2
                //*[@id="container"]/section[2]/span[1]/div/section/ul/li[1]/section/div/div[2]/div[1]/a/span
                //*[@id="container"]/section[2]/span[1]/div/section/ul/li[2]/section/div/div[2]/div[1]/a/span
                //*[@id="container"]/section[2]/span[1]/div/section/ul/li[3]/section/div/div[2]/div[1]/a/span
                //*[@id="container"]/section[2]/span[1]/div/section/ul/li[4]/section/div/div[2]/div[10]/a/span
                var courseExpr = navigator.Compile("count(//div/section/ul/li)");
                //var lExpr = navigator.Compile("count(//div/span)");
                var courseCount = Convert.ToInt32(navigator.Evaluate(courseExpr)); // 3 (nodes)
                for (var c = 1; c < courseCount + 1; c++)
                {
                    //*[@id="container"]/section[2]/span[1]/div/section/ul/li[1]/section/div/div[1]/h2
                    //*[@id="container"]/section[2]/span[1]/div/section/ul/li[2]/section/div/div/h2
                    var course = doc.DocumentNode.SelectNodes("//div/section/ul/li["+c.ToString()+"]/section/div/div/h2")[0].InnerText;
                    var lExpr = navigator.Compile("count(//div/section/ul/li["+c.ToString()+"]/section/div/div[2]/div)");
                    //var lExpr = navigator.Compile("count(//div/span)");
                    var lCount = Convert.ToInt32(navigator.Evaluate(lExpr)); // 3 (nodes)
                    for (var l = 1; l < lCount + 1; l++)
                    {
                        //*[@id="container"]/section[2]/span[1]/div/section/ul/li[1]/section/div/div[2]/div[1]/a/span
                        string time = doc.DocumentNode.SelectNodes("//div/section/ul/li["+c.ToString()+"]/section/div/div[2]/div["+l.ToString()+"]/a/span")[0].InnerText;
                        var cancelled = "";// doc.DocumentNode.SelectNodes("//div/div/div[2]/div/div[2]/div/div/div/div[" + c.ToString() + "]/section/table/tbody/tr[5]/td[5]")[0].InnerText;
                        if (cancelled != "Cancelled")
                        {
                            //*[@id="container"]/section[2]/span[1]/div/section/ul/li[1]/section/div/div[2]/div[1]/a/span
                            var league2 = doc.DocumentNode.SelectNodes("//div/section/ul/li[" + c.ToString() + "]/section/div/div[2]/div[" + l.ToString() + "]/a[@href]")[0].Attributes[1].Value;
                          
                            string raceUrl = "https://www.sportinglife.com" + league2;

                            HtmlDocument raceDoc = web.Load(raceUrl);
                            var raceNavigator = raceDoc.CreateNavigator();
                            //*[@id="container"]/section[2]/span[1]/div/div/section[2]/div[1]/div[2]/div[1]/div[1]/div[1]/span
                            //*[@id="container"]/section[2]/span[1]/div/div/section[2]/div[1]/div[3]/div[1]/div[1]/div[1]/span
                            var raceExpr = raceNavigator.Compile("count(//div/div/section[2]/div[1]/div)");
                            var raceCount = Convert.ToInt32(raceNavigator.Evaluate(raceExpr));
                            //*[@id="u8819883792923676"]/div/div/h1/span[1]

                            //for (var i = 0; i < raceCount; i++)
                            //{
                            //div/div/section[2]/div[1]/div[3]/div[1]/div[3]/div[1]/span/span[1]
                         //*[@id="container"]/section[2]/span[1]/div/div/div[1]/div[1]/div[1]/div[1]/div
                            var horseExpr = raceNavigator.Compile("count(//div/div/section[2]/div[1]/div)");
                            var horseCount = Convert.ToInt32(raceNavigator.Evaluate(horseExpr));
                            if (horseCount == 0)
                            {
                                //*[@id="container"]/section[2]/span[1]/div/div/div[1]/div[5]
                                horseExpr = raceNavigator.Compile("count(//span[1]/div/div/div[1]/div)");
                                horseCount = Convert.ToInt32(raceNavigator.Evaluate(horseExpr));
                                for (var i = 1; i <= horseCount; i++)
                                {
                                    string hometeam;
                                    string homeGoals = null;
                                    string odds = "";
                                    //*[@id="container"]/section[2]/span[1]/div/div/div[1]/div[1                      ]/div[1]/div[2]/div[1]/span/span[1]
                                           //*[@id="container"]/section[2]/span[1]/div/div/div[1]/div[6                    ]/div[1]/div[2]/div[1]/span/span[1]
                                    hometeam = raceDoc.DocumentNode.SelectNodes("//div/div/div[1]/div["+ (i).ToString() + "]/div[1]/div[2]/div[1]/span/span[1]")[0].InnerText;
                                    homeGoals = "0";

                                    var blah = UpdateSql(day, hometeam, course, homeGoals, time, odds);

                                }
                            }
                            else
                            {
                                for (var i = 2; i < horseCount + 1; i++)
                                {
                                    //int count = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[1]/ul/li[" + i.ToString() + "]/article/div/span[1]/span/span").Count();
                                    string hometeam;
                                    string awayteam;
                                    string awayGoals = null;
                                    string homeGoals = null;
                                    string odds = "";

                                    if (horseCount == 1)
                                    {
                                        var nodes = doc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[1]").ToList();

                                        if (nodes[0].FirstChild.Name == "a")
                                        {
                                            hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[1]/span/span")[0].InnerText;
                                            awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[3]/span/span")[0].InnerText;
                                            homeGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ ul/li/a/article/div/span[1]/span[2]/span")[0].InnerText;
                                            awayGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ ul/li/a/article/div/span[3]/span[2]/span")[0].InnerText;
                                            if (awayGoals != null)
                                            {
                                                hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[1]/span[1]/span/span")[0].InnerText;
                                                awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[3]/span[1]/span/span")[0].InnerText;
                                            }
                                        }
                                        else
                                        {
                                            hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/article/div/span[1]/span/span")[0].InnerText;
                                            awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/article/div/span[3]/span/span")[0].InnerText;
                                        }
                                    }
                                    else
                                    {
                                        var nodes = doc.DocumentNode.SelectNodes("//div/div/div[2]").ToList();

                                        if (nodes[0].FirstChild.Name == "a")
                                        {
                                            //*[@id="u09206958598642645"]/div/div[3]/div/div/span/div/div[1                   ]/ul/li[1                         ]/a/article/div/span[1]/span/span
                                            hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span/span")[0].InnerText;
                                            awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span/span")[0].InnerText;
                                            if (nodes[0].FirstChild.FirstChild.FirstChild.FirstChild.FirstChild.FirstChild.Name == "span")
                                            {
                                                homeGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span[2]/span")[0].InnerText;
                                                awayGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span[2]/span")[0].InnerText;
                                            }
                                            if (homeGoals != null)
                                            {
                                                hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span[1]/span/span")[0].InnerText;
                                                awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span[1]/span/span")[0].InnerText;

                                            }
                                        }
                                        else
                                        {
                                            //*[@id="container"]/section[2]/span[1]/div/div/section[2]/div[1]/div[2]/div[1]/div[1]/div[1]/span
                                            //*[@id="container"]/section[2]/span[1]/div/div/section[2]/div[1]/div[3]/div[1]/div[1]/div[1]/span
                                            //string result = raceDoc.DocumentNode.SelectNodes("//div/div/section[2]/div[1]/div["+i.ToString()+"]/div[1]/div[1]/div[1]/span")[0].InnerText;
                                            if (raceDoc.DocumentNode.SelectNodes("//div/div/section[2]/div[1]/div[1]/span")[0].InnerText == "Final Result")
                                            {
                                                var rowExpr = raceNavigator.Compile("count(//div/div/section[2]/div[1]/div[" + i.ToString() + "]/div[1]/div)");
                                                //var lExpr = navigator.Compile("count(//div/span)");
                                                var rowCount = Convert.ToInt32(raceNavigator.Evaluate(rowExpr));
                                                //*[@id="container"]/section[2]/span[1]/div/div/section[2]/div[1]/div[2]/div[1]/div[3]/div[1]/span/span[1]
                                                //*[@id="container"]/section[2]/span[1]/div/div/section[2]/div[1]/div[7]/div[1]/div[1]/div[1]/span
                                                //*[@id="container"]/section[2]/span[1]/div/div/section[2]/div[1]/div[7                      ]/div[1]/div[2]/div[1]/span/span[1]
                                                if (rowCount > 3)
                                                {
                                                    hometeam = raceDoc.DocumentNode.SelectNodes("//div/div/section[2]/div[1]/div[" + i.ToString() + "]/div[1]/div[3]/div[1]/span/span[1]")[0].InnerText;
                                                    homeGoals = raceDoc.DocumentNode.SelectNodes("//div/div/section[2]/div[1]/div[" + i.ToString() + "]/div[1]/div[1]/div[1]/span")[0].InnerText;
                                                }
                                                else
                                                {
                                                    hometeam = raceDoc.DocumentNode.SelectNodes("//div/div/section[2]/div[1]/div[" + i.ToString() + "]/div[1]/div[2]/div[1]/span/span[1]")[0].InnerText;
                                                    homeGoals = "0";
                                                }
                                                //homeGoals = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[1]")[0].InnerHtml;
                                                //homeGoals = Regex.Match(homeGoals, @"\(([^)]*)\)").Groups[1].Value;
                                                // odds = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[3]")[0].InnerHtml;
                                            }
                                            else
                                            {
                                                hometeam = raceDoc.DocumentNode.SelectNodes("//div/div/section[2]/div[1]/div[" + i.ToString() + "]/div[1]/div[3]/div[1]/span/span[1]")[0].InnerText;
                                                homeGoals = "0";
                                                // odds = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[3]")[0].InnerHtml;

                                            }
                                            //awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/article/div/span[3]/span/span")[0].InnerText;
                                        }
                                    }
                                    //Program program = new Program();

                                    var blah = UpdateSql(day, hometeam, course, homeGoals, time, odds);
                                    //Console.WriteLine(hometeam + "vs" + awayteam);
                                }
                            }
                        }
                    }
                }

            }
            return "";
        }

        
        public bool UpdateSql(DateTime date, string horseName, string course, string finish,string time,string odds)
        {
       
            if (course != "")
            {
                try
                {
                    SqlConnection connection = new SqlConnection("Data Source=localhost;Initial Catalog=DogRacing;Integrated Security=True");
                    SqlDataReader rdr = null;

                    // 1.  create a command object identifying
                    //     the stored procedure
                    SqlCommand cmd = new SqlCommand("AddRace", connection);

                    // 2. set the command object so it knows
                    //    to execute a stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Connection.Open();
                    // 3. add parameter to command, which
                    //    will be passed to the stored procedure
                    cmd.Parameters.Add(new SqlParameter("@DogName", horseName));
                    cmd.Parameters.Add(new SqlParameter("@Course", course));
                    cmd.Parameters.Add(new SqlParameter("@Date", date));
                    cmd.Parameters.Add(new SqlParameter("@Finish", finish));
                    cmd.Parameters.Add(new SqlParameter("@Time", time));
                    cmd.Parameters.Add(new SqlParameter("@Odds", odds));
                    // execute the command
                    rdr = cmd.ExecuteReader();

                    rdr.Close();
                    cmd.Dispose();

                    
                    return true;
                }
                catch (Exception ex)
                {

                    return false;
                }
            }
            return true;
        }
    }
}
