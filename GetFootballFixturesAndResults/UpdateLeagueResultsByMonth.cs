﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GetFootballFixturesAndResults
{
    public class UpdateLeagueResultsByMonth
    {
        public string UpdateLeagueResults(DateTime monthDate,string league)
        {
            //league = "premier-league";

        // for (DateTime day = fromDate; day.Date <= toDate; day = day.AddDays(1))
        //{

            string url = "https://www.bbc.co.uk/sport/football/"+league+"/scores-fixtures/" + monthDate.ToString("yyyy-MM");
            var web = new HtmlAgilityPack.HtmlWeb();
            HtmlDocument doc = web.Load(url);
            var navigator = doc.CreateNavigator();

            List<HtmlNode> toftitle = doc.DocumentNode.Descendants().Where(x => (x.Name == "div" && x.Attributes["class"] != null)).ToList();
            // html / body / div[5] / div[2] / div / div[3] / div / div / span / div / div[1] / h3
            // html / body / div[5] / div[2] / div / div[3] / div / div / span / div / div[2] / h3
            //*[@id="u1011088933452986"]/div/div[3]/div/div/span/div/div[2]/h3
            //*[@id="u2194635885828291"]/div/div[3]/div/div/span/div/div[1]/h3
            var lExpr = navigator.Compile("count(//div/div/div[3]/div/div/span/div/div)");
            var lCount = Convert.ToInt32(navigator.Evaluate(lExpr))+1; // 3 (nodes)
            for (var l = 1; l < lCount; l++)
            {
                var expr = navigator.Compile("count(//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li)");
                var count = Convert.ToInt32(navigator.Evaluate(expr)); // 3 (nodes)
                                                                       //var enumDisplayStatus = (league)l;
                                                                       //Console.WriteLine(enumDisplayStatus);
                string date = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/h3")[0].InnerText;
                
                for (var i = 0; i < count; i++)
                {
                    //int count = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[1]/ul/li[" + i.ToString() + "]/article/div/span[1]/span/span").Count();
                    string hometeam;
                    string awayteam;
                    string awayGoals = null;
                    string homeGoals = null;

                    if (count == 1)
                    {
                        var nodes = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li").ToList();

                        //HtmlNode node;// = "a";
                        //node.NodeType = "a";
                        if (nodes[0].FirstChild.Name == "a")
                        {
                            hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[1]/span/span")[0].InnerText;
                            awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[3]/span/span")[0].InnerText;
                            homeGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ ul/li/a/article/div/span[1]/span[2]/span")[0].InnerText;
                            awayGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ ul/li/a/article/div/span[3]/span[2]/span")[0].InnerText;
                            if (awayGoals != null)
                            {
                                hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[1]/span[1]/span/span")[0].InnerText;
                                awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[3]/span[1]/span/span")[0].InnerText;
                            }
                        }
                        else
                        {
                            hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/article/div/span[1]/span/span")[0].InnerText;
                            awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/article/div/span[3]/span/span")[0].InnerText;
                        }
                    }
                    else
                    {
                        var nodes = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]").ToList();

                        if (nodes[0].FirstChild.Name == "a")
                        {
                            //*[@id="u09206958598642645"]/div/div[3]/div/div/span/div/div[1                   ]/ul/li[1                         ]/a/article/div/span[1]/span/span
                                              //*[@id="u1011088933452986"]/div/div[3]/div/div/span/div/div[1             ]/ul/li[1]/a/article/div/span[1]/span[1]/span/span
                            hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span/span")[0].InnerText;
                            awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span/span")[0].InnerText;
                            if (nodes[0].FirstChild.FirstChild.FirstChild.FirstChild.FirstChild.FirstChild.Name == "span")
                            {
                                homeGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span[2]/span")[0].InnerText;
                                awayGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span[2]/span")[0].InnerText;
                            }
                            if (homeGoals != null)
                            {
                                hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span[1]/span/span")[0].InnerText;
                                awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span[1]/span/span")[0].InnerText;

                            }
                        }
                        else
                        {
                            //*[@id="u68582870311485085"]/div/div[3]/div/div/span/div/div[8                   ]/ul/li[10]/a/article/div/span[1]/span[1]/span/span
                            hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/article/div/span[1]/span/span")[0].InnerText;
                            awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/article/div/span[3]/span/span")[0].InnerText;
                        }
                    }
                    //Program program = new Program();
                    date = new string (date.Where(c => char.IsDigit(c)).ToArray());
                    DateTime day = Convert.ToDateTime(monthDate.ToString("yyyy-MM-") +date);
                    var blah = UpdateSql(day, hometeam, awayteam, league, homeGoals, awayGoals);
                    Console.WriteLine(hometeam + "vs" + awayteam);
                }
            }
            //}

            Console.WriteLine("Hello World!");
            return "";
        }
        public bool UpdateSql(DateTime date, string homeTeamName, string awayTeamName, string league, string? homeGoals, string? awayGoals)
        {
            int awayTeamId = 0;
            int homeTeamId = 0;
            int leagueId = 0;
            string fixtureId = "";

            switch (league)
            {
                case "premier-league":
                    leagueId = 1;
                    break;
                case "championship":
                    leagueId = 2;
                    break;
                case "league-one":
                    leagueId = 3;
                    break;
                case "league-two":
                    leagueId = 4;
                    break;
                case "scottish-premiership":
                    leagueId = 6;
                    break;
                case "scottish-championship":
                    leagueId = 7;
                    break;
                default:
                    break;
            }
            if (leagueId != 0)
            {
                try
                {
                    SqlConnection connection = new SqlConnection("Data Source=localhost;Initial Catalog=Football;Integrated Security=True");
                    SqlCommand command = new SqlCommand("SELECT dbo.GetTeamIdFromName('" + homeTeamName + "') as homeTeamId,dbo.GetTeamIdFromName('" + awayTeamName + "') as awayTeamId", connection);
                    //SqlCommand command = new SqlCommand("SELECT * FROM Team where name in ('" + homeTeamName + "','" + awayTeamName + "')", connection);
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            // if (reader["name"].ToString() == homeTeamName)
                            // {
                            homeTeamId = Convert.ToInt32(reader["homeTeamId"]);
                            // }
                            // else if (reader["name"].ToString() == awayTeamName)
                            // {
                            awayTeamId = Convert.ToInt32(reader["awayTeamId"]);
                            // }
                            //Console.WriteLine(String.Format("{0}", reader["name"].ToString()));

                        }
                    }

                    //SqlConnection conn = null;
                    SqlDataReader rdr = null;
                    //conn = new SqlConnection("Server=(local);DataBase=Northwind;Integrated Security=SSPI");
                    //conn.Open();

                    // 1.  create a command object identifying
                    //     the stored procedure
                    SqlCommand cmd = new SqlCommand("AddFixture", connection);

                    // 2. set the command object so it knows
                    //    to execute a stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    // 3. add parameter to command, which
                    //    will be passed to the stored procedure
                    cmd.Parameters.Add(new SqlParameter("@HomeTeamId", homeTeamId));
                    cmd.Parameters.Add(new SqlParameter("@AwayTeamId", awayTeamId));
                    cmd.Parameters.Add(new SqlParameter("@Date", date));
                    cmd.Parameters.Add(new SqlParameter("@LeagueId", leagueId));
                    // execute the command
                    rdr = cmd.ExecuteReader();

                    // iterate through results, printing each to console
                    while (rdr.Read())
                    {
                        Console.WriteLine("Product: {0}", rdr["FixtureId"]);
                        fixtureId = rdr["FixtureId"].ToString();
                    }
                    rdr.Close();
                    cmd.Dispose();
                    //cmd.Connection.Close();
                    if (homeGoals != null)
                    {
                        SqlDataReader rdr2 = null;
                        // 1.  create a command object identifying
                        //     the stored procedure
                        SqlCommand cmd2 = new SqlCommand("AddResult", connection);

                        // 2. set the command object so it knows
                        //    to execute a stored procedure
                        cmd2.CommandType = CommandType.StoredProcedure;

                        // 3. add parameter to command, which
                        //    will be passed to the stored procedure
                        cmd2.Parameters.Add(new SqlParameter("@HomeGoals", homeGoals));
                        cmd2.Parameters.Add(new SqlParameter("@AwayGoals", awayGoals));
                        cmd2.Parameters.Add(new SqlParameter("@FixtureId", fixtureId));
                        // execute the command
                        rdr2 = cmd2.ExecuteReader();
                        /*string querystr = "INSERT INTO Result([HomeTeamGoals]      ,[AwayTeamGoals]      ,[FixtureId])VALUES( @HomeTeamGoals, @AwayTeamGoals,@FixtureId)";
                        SqlCommand query = new SqlCommand(querystr, connection);

                        query.Parameters.Add("@HomeTeamGoals", SqlDbType.Int);
                        query.Parameters["@HomeTeamGoals"].Value = homeGoals;
                        query.Parameters.Add("@AwayTeamGoals", SqlDbType.Int);
                        query.Parameters["@AwayTeamGoals"].Value = awayGoals;
                        query.Parameters.Add("@FixtureId", SqlDbType.Int);
                        query.Parameters["@FixtureId"].Value = fixtureId;
                        query.ExecuteNonQuery();*/
                    }

                    command.Connection.Close();
                    return true;
                }
                catch (Exception ex)
                {

                    return false;
                }
            }
            return true;
        }
    }
}
