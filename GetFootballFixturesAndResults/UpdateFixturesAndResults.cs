﻿using HtmlAgilityPack;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace GetFootballFixturesAndResults
{
    public class UpdateFixturesAndResults
    {
        public string sqlConnection;
        SqlConnection connection;
        //AddToDb addToDb;// = new AddToDb() 
        public ILogger logger { get; set; }
        public UpdateFixturesAndResults(string sqlConnection, ILogger logger)
        {
            this.sqlConnection = sqlConnection;
            this.connection = new SqlConnection(sqlConnection);
            this.logger = logger;
          //  this.addToDb = new AddToDb(sqlConnection, logger);
        }
        public string AddNewFixturesAndResults(DateTime fromDate,DateTime toDate)
        {
            //DateTime fromDate = new DateTime(2022, 08, 07);
            //DateTime toDate = new DateTime(2022, 09, 28);

            for (DateTime day = fromDate; day.Date <= toDate; day = day.AddDays(1))
            {
                string url = "https://www.bbc.co.uk/sport/football/scores-fixtures/" + day.ToString("yyyy-MM-dd");
                var web = new HtmlAgilityPack.HtmlWeb();
                HtmlDocument doc = web.Load(url);
                var navigator = doc.CreateNavigator();

                List<HtmlNode> toftitle = doc.DocumentNode.Descendants().Where(x => (x.Name == "div" && x.Attributes["class"] != null)).ToList();
                var lExpr = navigator.Compile("count(//div/div/div[3]/div/div/span/div/div)");
                var lCount = Convert.ToInt32(navigator.Evaluate(lExpr)); // 3 (nodes)
                for (var l = 1; l < lCount; l++)
                {
                    var expr = navigator.Compile("count(//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li)");
                    var count = Convert.ToInt32(navigator.Evaluate(expr)); // 3 (nodes)
                                                                           //var enumDisplayStatus = (league)l;
                                                                           //Console.WriteLine(enumDisplayStatus);
                    string league = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/h3")[0].InnerText;
                    Console.WriteLine(league);
                    for (var i = 0; i < count; i++)
                    {
                        //int count = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[1]/ul/li[" + i.ToString() + "]/article/div/span[1]/span/span").Count();
                        string hometeam;
                        string awayteam;
                        string awayGoals = null;
                        string homeGoals = null;

                        if (count == 1)
                        {
                            var nodes = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li").ToList();

                            //HtmlNode node;// = "a";
                            //node.NodeType = "a";
                            if (nodes[0].FirstChild.Name == "a")
                            {
                                hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[1]/span/span")[0].InnerText;
                                awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[3]/span/span")[0].InnerText;
                                homeGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ ul/li/a/article/div/span[1]/span[2]/span")[0].InnerText;
                                awayGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ ul/li/a/article/div/span[3]/span[2]/span")[0].InnerText;
                                if (awayGoals != null)
                                {
                                    hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[1]/span[1]/span/span")[0].InnerText;
                                    awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[3]/span[1]/span/span")[0].InnerText;
                                }
                            }
                            else
                            {
                                hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/article/div/span[1]/span/span")[0].InnerText;
                                awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/article/div/span[3]/span/span")[0].InnerText;
                            }
                        }
                        else
                        {
                            var nodes = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]").ToList();

                            if (nodes[0].FirstChild.Name == "a")
                            {
                                //*[@id="u09206958598642645"]/div/div[3]/div/div/span/div/div[1                   ]/ul/li[1                         ]/a/article/div/span[1]/span/span
                                hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span/span")[0].InnerText;
                                awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span/span")[0].InnerText;
                                if (nodes[0].FirstChild.FirstChild.FirstChild.FirstChild.FirstChild.FirstChild.Name == "span")
                                {
                                    homeGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span[2]/span")[0].InnerText;
                                    awayGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span[2]/span")[0].InnerText;
                                }
                                if (homeGoals != null)
                                {
                                    hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span[1]/span/span")[0].InnerText;
                                    awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span[1]/span/span")[0].InnerText;

                                }
                            }
                            else
                            {
                                //*[@id="u68582870311485085"]/div/div[3]/div/div/span/div/div[8                   ]/ul/li[10]/a/article/div/span[1]/span[1]/span/span
                                hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/article/div/span[1]/span/span")[0].InnerText;
                                awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/article/div/span[3]/span/span")[0].InnerText;
                            }
                        }
                        //Program program = new Program();
                        var blah = UpdateSql(day, hometeam, awayteam, league, homeGoals, awayGoals);
                        Console.WriteLine(hometeam + "vs" + awayteam);
                    }
                }
            }

            Console.WriteLine("Hello World!");
            return "";
        }


        public bool UpdateSql(DateTime date, string homeTeamName, string awayTeamName, string league, string? homeGoals, string? awayGoals)
        {
            int awayTeamId = 0;
            int homeTeamId = 0;
            int leagueId = 0;
            string fixtureId = "";

            switch (league)
            {
                case "Premier League":
                    leagueId = 1;
                    break;
                case "Championship":
                    leagueId = 2;
                    break;
                case "League One":
                    leagueId = 3;
                    break;
                case "League Two":
                    leagueId = 4;
                    break;
                case "Scottish Premiership":
                    leagueId = 6;
                    break;
                case "Scottish Championship":
                    leagueId = 7;
                    break;
                default:
                    break;
            }
            if (leagueId != 0)
            {
                try
                {
                    //SqlConnection connection = new SqlConnection("Data Source=localhost;Initial Catalog=Football;Integrated Security=True");
                    SqlCommand command = new SqlCommand("SELECT dbo.GetTeamIdFromName('" + homeTeamName + "') as homeTeamId,dbo.GetTeamIdFromName('" + awayTeamName + "') as awayTeamId", connection);
                    //SqlCommand command = new SqlCommand("SELECT * FROM Team where name in ('" + homeTeamName + "','" + awayTeamName + "')", connection);
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            // if (reader["name"].ToString() == homeTeamName)
                            // {
                            homeTeamId = Convert.ToInt32(reader["homeTeamId"]);
                            // }
                            // else if (reader["name"].ToString() == awayTeamName)
                            // {
                            awayTeamId = Convert.ToInt32(reader["awayTeamId"]);
                            // }
                            //Console.WriteLine(String.Format("{0}", reader["name"].ToString()));

                        }
                    }

                    //SqlConnection conn = null;
                    SqlDataReader rdr = null;
                    //conn = new SqlConnection("Server=(local);DataBase=Northwind;Integrated Security=SSPI");
                    //conn.Open();

                    // 1.  create a command object identifying
                    //     the stored procedure
                    SqlCommand cmd = new SqlCommand("AddFixture", connection);

                    // 2. set the command object so it knows
                    //    to execute a stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    // 3. add parameter to command, which
                    //    will be passed to the stored procedure
                    cmd.Parameters.Add(new SqlParameter("@HomeTeamId", homeTeamId));
                    cmd.Parameters.Add(new SqlParameter("@AwayTeamId", awayTeamId));
                    cmd.Parameters.Add(new SqlParameter("@Date", date));
                    cmd.Parameters.Add(new SqlParameter("@LeagueId", leagueId));
                    // execute the command
                    rdr = cmd.ExecuteReader();

                    // iterate through results, printing each to console
                    while (rdr.Read())
                    {
                        Console.WriteLine("Product: {0}", rdr["FixtureId"]);
                        fixtureId = rdr["FixtureId"].ToString();
                    }
                    rdr.Close();
                    cmd.Dispose();
                    //cmd.Connection.Close();
                    if (homeGoals != null)
                    {
                        SqlDataReader rdr2 = null;
                        // 1.  create a command object identifying
                        //     the stored procedure
                        SqlCommand cmd2 = new SqlCommand("AddResult", connection);

                        // 2. set the command object so it knows
                        //    to execute a stored procedure
                        cmd2.CommandType = CommandType.StoredProcedure;

                        // 3. add parameter to command, which
                        //    will be passed to the stored procedure
                        cmd2.Parameters.Add(new SqlParameter("@HomeGoals", homeGoals));
                        cmd2.Parameters.Add(new SqlParameter("@AwayGoals", awayGoals));
                        cmd2.Parameters.Add(new SqlParameter("@FixtureId", fixtureId));
                        // execute the command
                        rdr2 = cmd2.ExecuteReader();
                        /*string querystr = "INSERT INTO Result([HomeTeamGoals]      ,[AwayTeamGoals]      ,[FixtureId])VALUES( @HomeTeamGoals, @AwayTeamGoals,@FixtureId)";
                        SqlCommand query = new SqlCommand(querystr, connection);

                        query.Parameters.Add("@HomeTeamGoals", SqlDbType.Int);
                        query.Parameters["@HomeTeamGoals"].Value = homeGoals;
                        query.Parameters.Add("@AwayTeamGoals", SqlDbType.Int);
                        query.Parameters["@AwayTeamGoals"].Value = awayGoals;
                        query.Parameters.Add("@FixtureId", SqlDbType.Int);
                        query.Parameters["@FixtureId"].Value = fixtureId;
                        query.ExecuteNonQuery();*/
                    }

                    command.Connection.Close();
                    return true;
                }
                catch (Exception ex)
                {

                    return false;
                }
            }
            return true;
        }
    }
}