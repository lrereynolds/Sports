using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.Azure.WebJobs.Host;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Newtonsoft.Json;

namespace SendEmailOfPredictions
{
    public static class Function1
    {
        [FunctionName("Function1")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            ILogger log,
            ExecutionContext context)
        {
            log.LogInformation($"C# Http trigger function executed at: {DateTime.Now}");
            try
            {
                CreateQueueIfNotExists(log, context);
            }catch(Exception ex)
            {
                log.LogError(ex.Message);
            }
            for (int i = 1; i <= 5; i++)
            {
                string randomStr = Guid.NewGuid().ToString();
                var serializeJsonObject = JsonConvert.SerializeObject(
                                             new
                                             {
                                                 ID = randomStr,
                                                 Content = $"<html><body><h2> This is a Sample HTML content {i}! </h2></body></html>"
                                             });

                CloudStorageAccount storageAccount = GetCloudStorageAccount(context);
                CloudQueueClient cloudQueueClient = storageAccount.CreateCloudQueueClient();
                CloudQueue cloudQueue = cloudQueueClient.GetQueueReference("email-queue");
                var cloudQueueMessage = new CloudQueueMessage(serializeJsonObject);
                try
                {
                    await cloudQueue.AddMessageAsync(cloudQueueMessage);
                }catch(Exception ex)
                {
                    log.LogError(ex.Message);
                }
            }

            return new OkObjectResult("UploadEmailMessagesHttpTriggerFunction executed successfully!!");
        }

        private static void CreateQueueIfNotExists(ILogger logger, ExecutionContext executionContext)
        {
            CloudStorageAccount storageAccount = GetCloudStorageAccount(executionContext);
            CloudQueueClient cloudQueueClient = storageAccount.CreateCloudQueueClient();
            string[] queues = new string[] { "email-queue" };
            foreach (var item in queues)
            {
                CloudQueue cloudQueue = cloudQueueClient.GetQueueReference(item);
                cloudQueue.CreateIfNotExistsAsync();
            }
        }
        private static CloudStorageAccount GetCloudStorageAccount(ExecutionContext executionContext)
        {
            var config = new ConfigurationBuilder().SetBasePath(executionContext.FunctionAppDirectory)
                                                   .AddJsonFile("local.settings.json", true, true)
                                                   .AddEnvironmentVariables()
                                                   .Build();
            CloudStorageAccount storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=http;AccountName=teststorage1983ye;AccountKey=l+wqCH2YiqbKGbTNbnlrF0x/StHOx4koC3NcoRJ/8vBBrHeCn3oEn0+T3V1QteCMYNsvBE4o3qTs+ASt9OoZvA==;EndpointSuffix=core.windows.net");
      //  https://teststorage1983yep.blob.core.windows.net/newcontainer?sp=r&st=2022-10-12T20:15:17Z&se=2022-10-13T04:15:17Z&spr=https&sv=2021-06-08&sr=c&sig=rCsHGYtHu6meazvJD2JrZiq3H%2FzJPPJ%2BgadWo5oMEN4%3D
              // CloudStorageAccount storageAccount = CloudStorageAccount.Parse(config["CloudStorageAccount"]);
            return storageAccount;
        }
    }
}
