using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using HorseRacing;
using Microsoft.Extensions.Logging;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace UpdateFixturesAndResults
{
    public class Function
    {
        
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string FunctionHandler(string input, ILambdaContext context)
        {
            using var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("NonHostConsoleApp.Program", LogLevel.Debug);
                    
            });
            ILogger logger = loggerFactory.CreateLogger<Function>();
            DateTime fromDate = DateTime.UtcNow;
            DateTime toDate = DateTime.UtcNow;
            string sqlConnection = "Server=tcp:mustangsally.database.windows.net,1433;Initial Catalog=Sport;Persist Security Info=False;User ID=admin2;Password=tuggiw-2haffe-Fangav;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            LambdaLogger.Log("Get data");
            AddRace addrace = new AddRace(sqlConnection,logger);
            var resultHorse = addrace.AddNewRace(fromDate, toDate);
            return input?.ToUpper();
        }
    }
}
