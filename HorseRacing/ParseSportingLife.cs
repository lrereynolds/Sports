﻿using HtmlAgilityPack;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HorseRacing
{
   public class ParseSportingLife
    {
        public string sqlConnection;
        SqlConnection connection;
        AddToDb addToDb;
        public ILogger logger { get; set; }
        public ParseSportingLife(string sqlConnection, ILogger logger)
        {
            this.sqlConnection = sqlConnection;
            this.connection = new SqlConnection(sqlConnection);
            this.logger = logger;
            this.addToDb = new AddToDb(sqlConnection, logger);
        }
        public async Task<string> AddNewRace(DateTime fromDate, DateTime toDate)
        {
            logger.LogInformation("Start add new race");
            DateTime s = fromDate;
            TimeSpan ts = new TimeSpan(00, 00, 0);
            fromDate = s.Date + ts;

            for (DateTime day = fromDate; day.Date <= toDate; day = day.AddDays(1))
            {
                logger.LogInformation("Add Race info for " + day.ToString("dd/MM/yy"));
                string url = "https://www.sportinglife.com/racing/racecards/" + day.ToString("yyyy-MM-dd");
                HtmlWeb web = new HtmlWeb();
                //HtmlDocument doc = await web.LoadFromWebAsync(url);

                HtmlDocument doc = web.Load(url);
                var navigator = doc.CreateNavigator();

                List<HtmlNode> toftitle = doc.DocumentNode.Descendants().Where(x => (x.Name == "div" && x.Attributes["class"] != null)).ToList();
                //*[@id="race-container"]/a/span
                //*[@id="race-container"]/a/div/span/span
                //*[@id="container"]/section[2]/span[1]/div/div/div[3]/div/div[1]/span[1]
                //*[@id="container"]/section[2]/span[1]/div/div/div[3]/div/div[1]/span[5]
                var courseExpr = navigator.Compile("count(//span[1]/div/div/div[3]/div/div[1]/span)");

                var courseCount = Convert.ToInt32(navigator.Evaluate(courseExpr)); // 3 (nodes)
                for (int c = 1; c < courseCount + 1; c++)
                {
                    //*[@id="race-container"]/a/span
                    //*[@id="race-container"]/a/span

                    var lExpr = navigator.Compile("count(//a/div/span/span)");

                    var lCount = Convert.ToInt32(navigator.Evaluate(lExpr)); // 3 (nodes)
                    for (var l = 0; l < lCount; l++)
                    {

                        string time = doc.DocumentNode.SelectNodes("//a/div/span/span")[l].InnerText;
                        //*[@id="u1436036466986943"]/div/div[2]/div/div[2]/div/div/div/div[4]/section/table/tbody/tr[2]/td[5]
                        //var cancelled = doc.DocumentNode.SelectNodes("//div/div/div[2]/div/div[2]/div/div/div/div[" + c.ToString() + "]/section/table/tbody/tr[" + l.ToString() + "]/td[5]")[0].InnerText;
                        //if (cancelled != "Cancelled")
                        //{
                            await GetRace(doc, web, day, time, c, l);
                           
                        // }
                    }
                }

            }
            return "";
        }
        public async Task<bool> GetRace(HtmlDocument doc, HtmlWeb web, DateTime day, string time, int c, int l)
        {
            //*[@id="race-container"]/a/span
            // html / body / div[1] / div / div / section[2] / span[1] / div / div[1] / section / ul / section / div / div / ul / li[1] / a / span
            //*[@id="race-container"]/a
            // html / body / div[1] / div / div / section[2] / span[1] / div / div / section / ul / section / div / div / ul / li[1] / a / span
            // html / body / div[1] / div / div / section[2] / span[1] / div / div / section / ul / section / div / div / ul / li[1] / a / span

            // html / body / div[1] / div / div / section[2] / span[1] / div / div / section / ul / section / div / div / ul / li[1] / a / span
            // html / body / div[1] / div / div / section[2] / span[1] / div / div / section / ul / section / div / div / ul / li[2] / a / span
            // html / body / div[1] / div / div / section[2] / span[1] / div / div / section / ul / section / div / div / ul / li[1] / a / span
            var league2 = doc.DocumentNode.SelectNodes("//div/ul/li["+c.ToString()+"]/a[@href]")[0].Attributes[0].Value;

            string raceUrl = "https://www.sportinglife.com";
            HtmlDocument raceDoc = new HtmlDocument();
            try
            {
                //raceDoc = await web.LoadFromWebAsync(raceUrl);
                raceDoc = web.Load(raceUrl + league2);
            }
            catch
            {
                //raceDoc = await web.LoadFromWebAsync(raceUrl);
                raceDoc = web.Load(raceUrl + league2);
            }
            var resultUrl = league2.Replace("racing/racecards", "racing/results").Replace("racecard/","");

            HtmlDocument raceResultDoc = new HtmlDocument();
            try
            {
                //raceDoc = await web.LoadFromWebAsync(raceUrl);
                raceResultDoc = web.Load(raceUrl + resultUrl);
            }
            catch
            {
                //raceDoc = await web.LoadFromWebAsync(raceUrl);
                raceResultDoc = web.Load(raceUrl + resultUrl);
            }

            var raceResultNavigator = raceResultDoc.CreateNavigator();
            //var raceExpr = raceResultNavigator.Compile("count(//div/div/div[2]/table/tbody/tr)");
            var raceNavigator = raceDoc.CreateNavigator();
            var raceExpr = raceNavigator.Compile("count(//div/div/div[2]/table/tbody/tr)");
            var raceCount = Convert.ToInt32(raceNavigator.Evaluate(raceExpr));
            //*[@id="horse-number-1"]
            //*[@id="125278167"]/div[1]/div[3]/div
            //*[@id="125278167"]
            /// html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[1]
            var horseExpr = raceNavigator.Compile("count(//section/section/div)");
            var horseCount = Convert.ToInt32(raceNavigator.Evaluate(horseExpr));

            for (var i = 1; i < horseCount + 1; i++)
            {
                //*[@id="container"]/section[2]/span[1]/div/div[1]/div[3]/div/div[1]/span[1]
                //*[@id="container"]/section[2]/span[1]/div/div[0]/div[3]/div/div[1]/span[1]
                var course = doc.DocumentNode.SelectNodes("//div/div[1]/div[3]/div/div[1]/span[1]")[0].InnerText;
                //*[@id="container"]/section[2]/span[1]/div/div[2]/div[1]/ul/h1
                string raceName = raceDoc.DocumentNode.SelectNodes("//div/div[2]/div[1]/ul/h1")[0].InnerText;
                //*[@id="ascot"]/div/div/div[2]/span[1]
                string going = doc.DocumentNode.SelectNodes("//div/div/div[2]/span[1]")[0].InnerText;
                //*[@id="race-container"]/a/span/span/span[4]
                string distance = doc.DocumentNode.SelectNodes("//a/span/span/span[4]")[0].InnerText;
                //int count = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[1]/ul/li[" + i.ToString() + "]/article/div/span[1]/span/span").Count();
                string hometeam;
                string awayteam;
                string awayGoals = null;
                string homeGoals = null;
                string odds = "";
                string age = "";
                string weight = "";
                string trainer = "";
                string jockey = "";
                //*[@id="container"]/section[2]/span[1]/div/div[2]/div[3]/div[1]/p[1]
                var resultExpr = raceResultNavigator.Compile("count(//span[1]/div/div[2]/div[3]/div[1]/p)");

                var resultCount = Convert.ToInt32(raceResultNavigator.Evaluate(resultExpr));
                if(resultCount>0)
                {
                  //  string result = raceResultDoc.DocumentNode.SelectNodes("//span[1]/div/div[2]/div[3]/div[1]/p[1]")[0].InnerText;
                    distance = raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[2]/dd")[0].InnerText;
                    going = "";// raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[3]/dd")[0].InnerText;
                    hometeam = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[3]")[0].InnerHtml;
                    homeGoals = "0";
                    odds = raceDoc.DocumentNode.SelectNodes("//div[" + c.ToString() + "]/div[1]/div[4]/span/span")[0].InnerHtml;
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[1] / div[1] / div[3] / div / span[1]
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[2] / div[1] / div[3] / div / span[1]
                    age = raceDoc.DocumentNode.SelectNodes("//div[" + c.ToString() + "]/div[1]/div[3]/div/span[1]")[0].InnerHtml;
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[1] / div[1] / div[3] / div / span[2]
                    weight = raceDoc.DocumentNode.SelectNodes("//div[" + c.ToString() + "]/div[1]/div[3]/div/span[2]")[0].InnerHtml;
                    //*[@id="129053281"]/div[1]/div[3]/div/a[2]/span
                    //*[@id="129053295"]/div[1]/div[3]/div/a[2]/span
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[2] / div[1] / div[3] / div / a[2] / span
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[1] / div[1] / div[3] / div / a[2] / span
                    trainer = raceDoc.DocumentNode.SelectNodes("//div[" + c.ToString() + "]/div[1]/div[3]/div/a[2]/span")[0].InnerHtml;
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[2] / div[1] / div[3] / div / a[1] / span
                    jockey = raceDoc.DocumentNode.SelectNodes("//div[" + c.ToString() + "]/div[1]/div[3]/div/a[1]/span")[0].InnerHtml;

                }else
                //catch (Exception)// if (raceDoc.DocumentNode.SelectNodes("//span[1]/div/div[2]/div[3]/div[1]/p[1]")[0].InnerText == "Result")
                {
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[1] / div[1] / div[3] / span / a
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[2] / div[1] / div[3] / span / a
                    //*[@id="129053318"]/div[1]/div[5]/div[1]/a
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / div[2] / div[1] / div[5] / div[1] / a
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / div[1] / div[1] / div[5] / div[1] / a
                    hometeam =  raceResultDoc.DocumentNode.SelectNodes("//div["+i.ToString()+"]/div[1]/div[5]/div[1]/a")[0].InnerHtml;
                    homeGoals = raceResultDoc.DocumentNode.SelectNodes("//div[1]/div["+i.ToString()+"]/div[1]/div[1]/span")[0].InnerHtml;

                   // homeGoals = Regex.Split(homeGoals, @"\(([^)]*)\)")[0];
                   // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[1] / div[1] / div[4] / span / span
                   // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[2] / div[1] / div[4] / span / span
                   // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / div[1] / div[1] / span / span
                   // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / div[2] / div[1] / span / span
                   odds = raceResultDoc.DocumentNode.SelectNodes("//div["+i.ToString()+ "]/div[1]/span/span")[0].InnerHtml;
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[1] / div[1] / div[3] / div / span[1]
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[2] / div[1] / div[3] / div / span[1]
                    //age = raceResultDoc.DocumentNode.SelectNodes("//div["+c.ToString()+"]/div[1]/div[3]/div/span[1]")[0].InnerHtml;
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[1] / div[1] / div[3] / div / span[2]
                    // weight = raceResultDoc.DocumentNode.SelectNodes("//div[" + c.ToString() + "]/div[1]/div[3]/div/span[2]")[0].InnerHtml;
                    //*[@id="129053281"]/div[1]/div[3]/div/a[2]/span
                    //*[@id="129053295"]/div[1]/div[3]/div/a[2]/span
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[2] / div[1] / div[3] / div / a[2] / span
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[1] / div[1] / div[3] / div / a[2] / span
                   // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / div[7] / div[2] / div[1] / div[1] / span[1] / span
                  // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / div[8] / div[2] / div[1] / div[1] / span[1] / span
                   trainer = raceResultDoc.DocumentNode.SelectNodes("//div["+i.ToString()+ "]/div[2]/div[1]/div[1]/span[1]/span")[0].InnerHtml;
                    // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[2] / div[1] / div[3] / div / a[1] / span
                   // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / div[19] / div[2] / div[1] / div[1] / span[2] / span[1]
                   // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / div[17] / div[2] / div[1] / div[1] / span[2] / span[1]
                    jockey = raceResultDoc.DocumentNode.SelectNodes("//div[" + i.ToString() + "]/div[2]/div[1]/div[1]/span[2]/span[1]")[0].InnerHtml;
                }
                
                var raceId = await addToDb.UpdateSql(day, hometeam, course, homeGoals, time, odds, going, distance, raceName, age, weight,
  trainer,
  jockey);
                // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[1] / div[5] / div[2] / div / table / tbody / tr[1] / td[3]
                // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[2] / div[5] / div[2] / div / table / tbody / tr[1] / td[3]
                // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[2] / div[5] / div[2] / div / table / tbody / tr[2] / td[3]
                // html / body / div[1] / div / div / section[2] / span[1] / div / div[2] / div[3] / div[1] / section / section / div[2] / div[5] / div[2] / div / table / tbody / tr[2]
                //*[@id="129053295-form"]/div/table/tbody/tr[2]
                //var formExpr = raceNavigator.Compile("count(//section/section/div["+i.ToString()+ "]/div[5]/div[2]/div/table/tbody/tr)");
                /*var formExpr = raceNavigator.Compile("count(//div[2]/div/table/tbody/tr)");

                var formCount = Convert.ToInt32(raceNavigator.Evaluate(formExpr));
                DateTime raceDay =DateTime.Now;
                int finish=0;

                odds = raceDoc.DocumentNode.SelectNodes("//div[" + c.ToString() + "]/div[1]/div[4]/span/span")[0].InnerHtml;
                string f = raceDoc.DocumentNode.SelectNodes("//div/table/tbody/tr[1]/td[7]")[0].InnerText;//*[@id="129053281-form"]/div/table/tbody/tr[1]/td[7]
                addToDb.AddRaceForm(raceDay, raceId, finish);*/
            }
            return true;
        }

    }
}
