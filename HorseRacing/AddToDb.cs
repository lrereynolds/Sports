﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace HorseRacing
{
    public class AddToDb
    {
        public string sqlConnection;
        SqlConnection connection;
        public ILogger logger { get; set; }
        public AddToDb(string sqlConnection, ILogger logger)
        {
            this.sqlConnection = sqlConnection;
            this.connection = new SqlConnection(sqlConnection);
            this.logger = logger;
        }
        public async Task<int> UpdateSql(DateTime date, string horseName, string course, string finish, string time, string odds, string going, string distance, string raceName, string age,
string weight,
string trainer,
 string jockey, string form = "")
        {
            int raceId=0;
            if (finish != null && finish != "0")
            {
                finish = finish;
            }
            if (course != "")
            {
                try
                {
                    //SqlConnection connection = new SqlConnection(sqlConnection);
                    //SqlDataReader rdr = null;

                    // 1.  create a command object identifying
                    //     the stored procedure
                    SqlCommand cmd = new SqlCommand("AddHorseRace", connection);

                    // 2. set the command object so it knows
                    //    to execute a stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;
                    //await cmd.Connection.OpenAsync();
                    cmd.Connection.Open();
                    // 3. add parameter to command, which
                    //    will be passed to the stored procedure
                    cmd.Parameters.Add(new SqlParameter("@horseName", horseName));
                    cmd.Parameters.Add(new SqlParameter("@Course", course));
                    cmd.Parameters.Add(new SqlParameter("@Date", date));
                    cmd.Parameters.Add(new SqlParameter("@Finish", finish));
                    cmd.Parameters.Add(new SqlParameter("@Time", time));
                    cmd.Parameters.Add(new SqlParameter("@Odds", odds));
                    cmd.Parameters.Add(new SqlParameter("@Going", going));
                    cmd.Parameters.Add(new SqlParameter("@Distance", distance));
                    cmd.Parameters.Add(new SqlParameter("@RaceName", raceName));
                    cmd.Parameters.Add(new SqlParameter("@Age", age));
                    cmd.Parameters.Add(new SqlParameter("@Weight", weight));
                    cmd.Parameters.Add(new SqlParameter("@Trainer", trainer));
                    cmd.Parameters.Add(new SqlParameter("@Jockey", jockey));
                    cmd.Parameters.Add(new SqlParameter("@Form", form));
                    // execute the command
                    //await cmd.ExecuteReaderAsync();
                    var rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        raceId = Convert.ToInt32(rdr["RaceId"].ToString());
                    }
                    //rdr.Close();
                    //await cmd.DisposeAsync();
                    //await connection.CloseAsync();

                    cmd.Dispose();
                    connection.Close();

                    return raceId;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex.Message.ToString());
                    return raceId;
                }
            }
            return raceId;
        }
        public async Task<int> AddRaceForm(DateTime date, int raceId, int finish)
        {
            
            if (finish != null && finish != 0)
            {
                finish = finish;
            }
           
                try
                {
                    //SqlConnection connection = new SqlConnection(sqlConnection);
                    //SqlDataReader rdr = null;

                    // 1.  create a command object identifying
                    //     the stored procedure
                    SqlCommand cmd = new SqlCommand("AddHorseRace", connection);

                    // 2. set the command object so it knows
                    //    to execute a stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;
                    //await cmd.Connection.OpenAsync();
                    cmd.Connection.Open();
                    // 3. add parameter to command, which
                    //    will be passed to the stored procedure
                    cmd.Parameters.Add(new SqlParameter("@Date", date));
                    cmd.Parameters.Add(new SqlParameter("@Finish", finish));
                    cmd.Parameters.Add(new SqlParameter("@Jockey", raceId));
                    // execute the command
                    //await cmd.ExecuteReaderAsync();
                    var rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        raceId = Convert.ToInt32(rdr["RaceId"].ToString());
                    }
                    //rdr.Close();
                    //await cmd.DisposeAsync();
                    //await connection.CloseAsync();

                    cmd.Dispose();
                    connection.Close();

                    return raceId;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex.Message.ToString());
                    return raceId;
                }
            
            return raceId;
        }
    }
}
