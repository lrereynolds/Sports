﻿using HtmlAgilityPack;
//using log4net.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace HorseRacing
{
    public class AddRace
    {
        public string sqlConnection;
        SqlConnection connection;
        AddToDb addToDb;// = new AddToDb() 
        public ILogger logger { get; set; }
        public AddRace(string sqlConnection, ILogger logger) {
            this.sqlConnection = sqlConnection;
            this.connection = new SqlConnection(sqlConnection);
            this.logger = logger;
            this.addToDb = new AddToDb(sqlConnection, logger);
        }
        public async Task<string> AddNewRace(DateTime fromDate, DateTime toDate)
        {
            logger.LogInformation("Start add new race");
            DateTime s = fromDate;
            TimeSpan ts = new TimeSpan(00, 00, 0);
            fromDate = s.Date + ts;

            //DateTime fromDate = new DateTime(2022, 08, 07);
            //DateTime toDate = new DateTime(2022, 09, 28);

            for (DateTime day = fromDate; day.Date <= toDate; day = day.AddDays(1))
            {
                logger.LogInformation("Add Race info for "+day.ToString("dd/MM/yy"));
                string url = "https://www.bbc.co.uk/sport/horse-racing/uk-ireland/results/" + day.ToString("yyyy-MM-dd");
                HtmlWeb web = new HtmlWeb();
                //HtmlDocument doc = await web.LoadFromWebAsync(url);

                HtmlDocument doc = web.Load(url);
                var navigator = doc.CreateNavigator();

                List<HtmlNode> toftitle = doc.DocumentNode.Descendants().Where(x => (x.Name == "div" && x.Attributes["class"] != null)).ToList();

                var courseExpr = navigator.Compile("count(//div/div/div[2]/div/div[2]/div/div/div/div)");
             
                var courseCount = Convert.ToInt32(navigator.Evaluate(courseExpr)); // 3 (nodes)
                for (int c = 1; c < courseCount + 1; c++)
                {
                    var lExpr = navigator.Compile("count(//div/div/div[2]/div/div[2]/div/div/div/div["+c.ToString()+"]/section/table/tbody/tr)");
                   
                    var lCount = Convert.ToInt32(navigator.Evaluate(lExpr)); // 3 (nodes)
                    for (var l = 1; l < lCount + 1; l++)
                    {
               
                        string time = doc.DocumentNode.SelectNodes("//div/div/div[2]/div/div[2]/div/div/div/div[" + c.ToString() + "]/section/table/tbody/tr[" + l.ToString() + "]/td[1]")[0].InnerText;
                                              //*[@id="u1436036466986943"]/div/div[2]/div/div[2]/div/div/div/div[4]/section/table/tbody/tr[2]/td[5]
                        var cancelled = doc.DocumentNode.SelectNodes("//div/div/div[2]/div/div[2]/div/div/div/div[" + c.ToString() + "]/section/table/tbody/tr["+l.ToString()+"]/td[5]")[0].InnerText;
                        if (cancelled != "Cancelled")
                        {
                           await GetRace(doc,web,day,time,c,l);
                            /*
                            var league2 = doc.DocumentNode.SelectNodes("//div/div/div[2]/div/div[2]/div/div/div/div[" + c.ToString() + "]/section/table/tbody/tr[" + l.ToString() + "]/td[5]/a[@href]")[0].Attributes[0].Value;
                            //string league = doc.DocumentNode.SelectNodes("//div/div[2]/div/div[2]/div/div/div/div[1]/section/table/tbody/tr[" + l.ToString() + "]]/td[5]/a")[0].InnerText;
                            //Console.WriteLine(league);
                            string raceUrl = "https://www.bbc.co.uk" + league2;
                            HtmlDocument raceDoc = new HtmlDocument();
                            try
                            {
                                raceDoc = web.Load(raceUrl);
                            }
                            catch
                            {
                                raceDoc = web.Load(raceUrl);
                            }
                                var raceNavigator = raceDoc.CreateNavigator();
                            //*[@id="sp-c-filter-contents"]/div/div[2]/table/tbody/tr[2]/td[3]
                            //*[@id="sp-c-filter-contents"]/div/div[2]/table/tbody/tr[8]/td[3]
                            var raceExpr = raceNavigator.Compile("count(//div/div/div[2]/table/tbody/tr)");
                            var raceCount = Convert.ToInt32(raceNavigator.Evaluate(raceExpr));
                            //*[@id="u8819883792923676"]/div/div/h1/span[1]
                            var course = raceDoc.DocumentNode.SelectNodes("//div/div/div/h1/span[1]")[0].InnerText;
                            //*[@id="sp-c-filter-contents"]/div/div[1]/dl/div[3]/dd
                            //*[@id="sp-c-filter-contents"]/div/div[1]/dl/div[2]/dd
                            //*[@id="sp-c-filter-contents"]/div/div[1]/h3
                            string going = raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[2]/dd")[0].InnerText;
                            string distance = raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[3]/dd")[0].InnerText;
                            var horseExpr = raceNavigator.Compile("count(//div/div/div[2]/table/tbody/tr)");
                            var horseCount = Convert.ToInt32(raceNavigator.Evaluate(horseExpr));
 
                            for (var i = 1; i < horseCount + 1; i++)
                            {
                                //int count = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[1]/ul/li[" + i.ToString() + "]/article/div/span[1]/span/span").Count();
                                string hometeam;
                                string awayteam;
                                string awayGoals = null;
                                string homeGoals = null;
                                string odds = "";

                                if (horseCount == 1)
                                {
                                    var nodes = doc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[1]").ToList();

                                    //HtmlNode node;// = "a";
                                    //node.NodeType = "a";
                                    if (nodes[0].FirstChild.Name == "a")
                                    {
                                        hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[1]/span/span")[0].InnerText;
                                        awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[3]/span/span")[0].InnerText;
                                        homeGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ ul/li/a/article/div/span[1]/span[2]/span")[0].InnerText;
                                        awayGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ ul/li/a/article/div/span[3]/span[2]/span")[0].InnerText;
                                        if (awayGoals != null)
                                        {
                                            hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[1]/span[1]/span/span")[0].InnerText;
                                            awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/a/article/div/span[3]/span[1]/span/span")[0].InnerText;
                                        }
                                    }
                                    else
                                    {
                                        hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/article/div/span[1]/span/span")[0].InnerText;
                                        awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li/article/div/span[3]/span/span")[0].InnerText;
                                    }
                                }
                                else
                                {
                                    var nodes = doc.DocumentNode.SelectNodes("//div/div/div[2]").ToList();

                                    if (nodes[0].FirstChild.Name == "a")
                                    {
                                        //*[@id="u09206958598642645"]/div/div[3]/div/div/span/div/div[1                   ]/ul/li[1                         ]/a/article/div/span[1]/span/span
                                        hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span/span")[0].InnerText;
                                        awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span/span")[0].InnerText;
                                        if (nodes[0].FirstChild.FirstChild.FirstChild.FirstChild.FirstChild.FirstChild.Name == "span")
                                        {
                                            homeGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span[2]/span")[0].InnerText;
                                            awayGoals = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span[2]/span")[0].InnerText;
                                        }
                                        if (homeGoals != null)
                                        {
                                            hometeam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[1]/span[1]/span/span")[0].InnerText;
                                            awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/a/article/div/span[3]/span[1]/span/span")[0].InnerText;

                                        }
                                    }
                                    else
                                    {

                                        string result = raceDoc.DocumentNode.SelectNodes("//div/div/div[1]/dl/div[1]/dd")[0].InnerText;
                                        if (raceDoc.DocumentNode.SelectNodes("//div/div/div[1]/dl/div[1]/dd")[0].InnerText== "Result")
                                        {
                                        hometeam = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[2]")[0].InnerHtml;
                                        homeGoals = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[1]")[0].InnerHtml;
                                            homeGoals = Regex.Split(homeGoals, @"\(([^)]*)\)")[0];
                                            odds = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[3]")[0].InnerHtml;
                                        }
                                        else
                                        {


                                            distance = raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[2]/dd")[0].InnerText;
                                            going = "";// raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[3]/dd")[0].InnerText;
                                            hometeam = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr["+i.ToString()+"]/td[3]")[0].InnerHtml;
                                            homeGoals = "0";
                                                           //*[@id="sp-c-filter-contents"]/div/div[2]/table/tbody/tr[1                   ]/td[4]
                                            odds = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[4]")[0].InnerHtml;

                                        }
                                        //awayteam = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[" + l.ToString() + "]/ul/li[" + (i + 1).ToString() + "]/article/div/span[3]/span/span")[0].InnerText;
                                    }
                                }
                                //Program program = new Program();
                                
                                var blah = UpdateSql(day, hometeam, course, homeGoals, time,odds,going,distance);
                                //Console.WriteLine(hometeam + "vs" + awayteam);
                            }*/
                        }
                    }
                }

            }
            return "";
        }
        public async Task<bool> GetRace(HtmlDocument doc, HtmlWeb web,DateTime day,string time,int c,int l)
        {
            var league2 = doc.DocumentNode.SelectNodes("//div/div/div[2]/div/div[2]/div/div/div/div[" + c.ToString() + "]/section/table/tbody/tr[" + l.ToString() + "]/td[5]/a[@href]")[0].Attributes[0].Value;

            string raceUrl = "https://www.bbc.co.uk" + league2;
            HtmlDocument raceDoc = new HtmlDocument();
            try
            {
                //raceDoc = await web.LoadFromWebAsync(raceUrl);
                raceDoc = web.Load(raceUrl);
            }
            catch
            {
                //raceDoc = await web.LoadFromWebAsync(raceUrl);
                raceDoc = web.Load(raceUrl);
            }
            var raceNavigator = raceDoc.CreateNavigator();
            //*[@id="sp-c-filter-contents"]/div/div[2]/table/tbody/tr[2]/td[3]
            //*[@id="sp-c-filter-contents"]/div/div[2]/table/tbody/tr[8]/td[3]
            var raceExpr = raceNavigator.Compile("count(//div/div/div[2]/table/tbody/tr)");
            var raceCount = Convert.ToInt32(raceNavigator.Evaluate(raceExpr));
            //*[@id="u8819883792923676"]/div/div/h1/span[1]
           // var course = raceDoc.DocumentNode.SelectNodes("//div/div/div/h1/span[1]")[0].InnerText;
           // string raceName = raceDoc.DocumentNode.SelectNodes("//div/div[1]/h3")[0].InnerText;
           // string going = raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[2]/dd")[0].InnerText;
            //string distance = raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[3]/dd")[0].InnerText;
            var horseExpr = raceNavigator.Compile("count(//div/div/div[2]/table/tbody/tr)");
            var horseCount = Convert.ToInt32(raceNavigator.Evaluate(horseExpr));

            for (var i = 1; i < horseCount + 1; i++)
            {
                var course = raceDoc.DocumentNode.SelectNodes("//div/div/div/h1/span[1]")[0].InnerText;
                string raceName = raceDoc.DocumentNode.SelectNodes("//div/div[1]/h3")[0].InnerText;
                string going = raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[2]/dd")[0].InnerText;
                string distance = raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[3]/dd")[0].InnerText;
                //int count = doc.DocumentNode.SelectNodes("//div/div/div[3]/div/div/span/div/div[1]/ul/li[" + i.ToString() + "]/article/div/span[1]/span/span").Count();
                string hometeam;
                string awayteam;
                string awayGoals = null;
                string homeGoals = null;
                string odds = "";
                string age="";
 string weight="";
 string trainer="";
  string jockey="";
                string form="";

                string result = raceDoc.DocumentNode.SelectNodes("//div/div/div[1]/dl/div[1]/dd")[0].InnerText;
                if (raceDoc.DocumentNode.SelectNodes("//div/div/div[1]/dl/div[1]/dd")[0].InnerText == "Result")
                {
                    hometeam = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[2]")[0].InnerHtml;
                    homeGoals = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[1]")[0].InnerHtml;
                    homeGoals = Regex.Split(homeGoals, @"\(([^)]*)\)")[0];
                    odds = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[3]")[0].InnerHtml;
                    age = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[4]")[0].InnerHtml;
                    weight = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[5]")[0].InnerHtml;
                    trainer = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[7]")[0].InnerHtml;
                    jockey = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[8]")[0].InnerHtml;
                }
                else
                {

                   
                    distance = raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[2]/dd")[0].InnerText;
                    going = "";// raceDoc.DocumentNode.SelectNodes("//div/div[1]/dl/div[3]/dd")[0].InnerText;
                    form = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[2]")[0].InnerHtml;
                    hometeam = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[3]")[0].InnerHtml;
                    homeGoals = "0";
                    //*[@id="sp-c-filter-contents"]/div/div[2]/table/tbody/tr[1                   ]/td[4]
                    odds = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[4]")[0].InnerHtml;
                    age = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[5]")[0].InnerHtml;
                    weight = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[6]")[0].InnerHtml;
                    trainer = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[7]")[0].InnerHtml;
                    jockey = raceDoc.DocumentNode.SelectNodes("//div/div/div[2]/table/tbody/tr[" + i.ToString() + "]/td[8]")[0].InnerHtml;

                }

                var blah = await addToDb.UpdateSql(day, hometeam, course, homeGoals, time, odds, going, distance, raceName, age, weight,
  trainer,
  jockey,form);
                //Console.WriteLine(hometeam + "vs" + awayteam);
            }
            return true;
        }

      /*  public async Task<bool> UpdateSql(DateTime date, string horseName, string course, string finish,string time,string odds, string going, string distance,string raceName, string age,
 string weight,
 string trainer,
  string jockey)
        {
       if(finish != null && finish != "0")
            {
                finish = finish;
            }
            if (course != "")
            {
                try
                {
                    //SqlConnection connection = new SqlConnection(sqlConnection);
                    //SqlDataReader rdr = null;

                    // 1.  create a command object identifying
                    //     the stored procedure
                    SqlCommand cmd = new SqlCommand("AddHorseRace", connection);

                    // 2. set the command object so it knows
                    //    to execute a stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;
                    //await cmd.Connection.OpenAsync();
                    cmd.Connection.Open();
                    // 3. add parameter to command, which
                    //    will be passed to the stored procedure
                    cmd.Parameters.Add(new SqlParameter("@horseName", horseName));
                    cmd.Parameters.Add(new SqlParameter("@Course", course));
                    cmd.Parameters.Add(new SqlParameter("@Date", date));
                    cmd.Parameters.Add(new SqlParameter("@Finish", finish));
                    cmd.Parameters.Add(new SqlParameter("@Time", time));
                    cmd.Parameters.Add(new SqlParameter("@Odds", odds));
                    cmd.Parameters.Add(new SqlParameter("@Going", going));
                    cmd.Parameters.Add(new SqlParameter("@Distance", distance));
                    cmd.Parameters.Add(new SqlParameter("@RaceName", raceName));
                    cmd.Parameters.Add(new SqlParameter("@Age", age));
                    cmd.Parameters.Add(new SqlParameter("@Weight", weight));
                    cmd.Parameters.Add(new SqlParameter("@Trainer", trainer));
                    cmd.Parameters.Add(new SqlParameter("@Jockey", jockey));
                    // execute the command
                    //await cmd.ExecuteReaderAsync();
                    cmd.ExecuteReader();

                    //rdr.Close();
                    //await cmd.DisposeAsync();
                    //await connection.CloseAsync();

                     cmd.Dispose();
                     connection.Close();

                    return true;
                }
                catch (Exception ex)
                {
                    logger.LogError(ex.Message.ToString());
                    return false;
                }
            }
            return true;
        }*/
    }
}
