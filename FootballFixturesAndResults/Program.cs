﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using FootballFixturesAndResults;
using GetFootballFixturesAndResults;
using HorseRacing;
using DogRacing;
using System.Diagnostics;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace FootballFixturesAndResults
{
    class Program
    {
        static void Main(string[] args)
        {
             using var loggerFactory = LoggerFactory.Create(builder =>
            {
                builder
                    .AddFilter("Microsoft", LogLevel.Warning)
                    .AddFilter("System", LogLevel.Warning)
                    .AddFilter("NonHostConsoleApp.Program", LogLevel.Debug)
                    .AddConsole();
            });
            ILogger logger = loggerFactory.CreateLogger<Program>();
            /*logger.LogInformation("Info Log");
            logger.LogWarning("Warning Log");
            logger.LogError("Error Log");
            logger.LogCritical("Critical Log");*/
            //Console.ReadLine();
            Stopwatch stopwatch = new Stopwatch();

            stopwatch.Start();
            List<DateTime> monthDates = new List<DateTime>();
            monthDates.Add(new DateTime(2022, 07, 01));
            monthDates.Add(new DateTime(2022, 08, 01));
            monthDates.Add(new DateTime (2022, 09, 01));
            monthDates.Add(new DateTime(2022, 10, 01));
            //monthDates.Add(new DateTime(2022, 11, 01));

            DateTime monthDate = new DateTime(2022, 10, 01);
            DateTime fromDate = new DateTime(2022, 11, 12);
            DateTime toDate = new DateTime(2022, 11, 20);
            UpdateLeagueResultsByMonth updateLeagueResultsByMonth = new();
         //   string sqlConnection = "Data Source=localhost;Initial Catalog=Sport;Integrated Security=True";
            string sqlConnection = "Server=tcp:mustangsally.database.windows.net,1433;Initial Catalog=Sport;Persist Security Info=False;User ID=admin2;Password=tuggiw-2haffe-Fangav;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;";
            string sqlConnection2 = "Data Source=localhost;Initial Catalog=Football;Integrated Security=True";
            UpdateFixturesAndResults updateFixturesAndResults = new(sqlConnection2, logger);
            AddRace addrace = new(sqlConnection,logger);
            ParseSportingLife parseSportingLife = new(sqlConnection, logger);
            AddDogRace addDogRace = new();
            //string league = "premier-league";
            string league = "championship";
            //string league = "league-one";
            List<string> leagues = new List<string>();
            leagues.Add("premier-league");
            leagues.Add("championship");
            leagues.Add("league-one");
            leagues.Add("league-two");
            leagues.Add("scottish-premiership");
            leagues.Add("scottish-championship");
            // var result = addDogRace.AddNewRace(fromDate, toDate);
            /*try
            {
                var resultHorse = addrace.AddNewRace(fromDate, toDate);
                //var resultHorse = parseSportingLife.AddNewRace(fromDate, toDate);
            }
            catch (Exception ex) {
                string i = ex.ToString();
            }*/
            /*foreach (var date in monthDates) {
                foreach (var league1 in leagues) {
                    var monthResult = updateLeagueResultsByMonth.UpdateLeagueResults(date, league1);
                } }*/
                var blah = updateFixturesAndResults.AddNewFixturesAndResults(fromDate, toDate);
           
            stopwatch.Stop();

            Console.WriteLine("Elapsed Time is {0} ms", stopwatch.ElapsedMilliseconds);
            Console.WriteLine("Hello World!");
        }

        public bool UpdateSql(DateTime date,string homeTeamName, string awayTeamName,string league, string? homeGoals,string? awayGoals)
        { 
            int awayTeamId=0;
            int homeTeamId=0;
            int leagueId=0;
            string fixtureId="";

            switch (league) { 
                case "Premier League":
                    leagueId = 1;
                    break;
                case "Championship":
                    leagueId = 2;
                    break;
                case "League One":
                    leagueId = 3;
                    break;
                case "League Two":
                    leagueId = 4;
                    break;
                case "Scottish Premiership":
                        leagueId = 6;
                    break;
                case "Scottish Championship":
                    leagueId = 7;
                    break;
                default:
                    break;
            }
            if (leagueId != 0)
            {
                try
                {
                    SqlConnection connection = new SqlConnection("Data Source=localhost;Initial Catalog=Football;Integrated Security=True");
                    SqlCommand command = new SqlCommand("SELECT dbo.GetTeamIdFromName('"+homeTeamName+"') as homeTeamId,dbo.GetTeamIdFromName('"+awayTeamName+"') as awayTeamId", connection);
                    //SqlCommand command = new SqlCommand("SELECT * FROM Team where name in ('" + homeTeamName + "','" + awayTeamName + "')", connection);
                    command.Connection.Open();
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                           // if (reader["name"].ToString() == homeTeamName)
                           // {
                                homeTeamId = Convert.ToInt32(reader["homeTeamId"]);
                           // }
                           // else if (reader["name"].ToString() == awayTeamName)
                           // {
                                awayTeamId = Convert.ToInt32(reader["awayTeamId"]);
                           // }
                            //Console.WriteLine(String.Format("{0}", reader["name"].ToString()));

                        }
                    }
                   
                    //SqlConnection conn = null;
                    SqlDataReader rdr = null;
                    //conn = new SqlConnection("Server=(local);DataBase=Northwind;Integrated Security=SSPI");
                    //conn.Open();

                    // 1.  create a command object identifying
                    //     the stored procedure
                    SqlCommand cmd = new SqlCommand("AddFixture", connection);

                    // 2. set the command object so it knows
                    //    to execute a stored procedure
                    cmd.CommandType = CommandType.StoredProcedure;

                    // 3. add parameter to command, which
                    //    will be passed to the stored procedure
                    cmd.Parameters.Add(new SqlParameter("@HomeTeamId", homeTeamId));
                    cmd.Parameters.Add(new SqlParameter("@AwayTeamId", awayTeamId));
                    cmd.Parameters.Add(new SqlParameter("@Date", date));
                    cmd.Parameters.Add(new SqlParameter("@LeagueId", leagueId));
                    // execute the command
                    rdr = cmd.ExecuteReader();
                    
                    // iterate through results, printing each to console
                    while (rdr.Read())
                    {
                        Console.WriteLine("Product: {0}", rdr["FixtureId"]);
                        fixtureId = rdr["FixtureId"].ToString();
                    }
                    rdr.Close();
                    cmd.Dispose();
                    //cmd.Connection.Close();
                    if (homeGoals != null) 
                    {
                        SqlDataReader rdr2 = null;
                        // 1.  create a command object identifying
                        //     the stored procedure
                        SqlCommand cmd2 = new SqlCommand("AddResult", connection);

                        // 2. set the command object so it knows
                        //    to execute a stored procedure
                        cmd2.CommandType = CommandType.StoredProcedure;

                        // 3. add parameter to command, which
                        //    will be passed to the stored procedure
                        cmd2.Parameters.Add(new SqlParameter("@HomeGoals", homeGoals));
                        cmd2.Parameters.Add(new SqlParameter("@AwayGoals", awayGoals));
                        cmd2.Parameters.Add(new SqlParameter("@FixtureId", fixtureId));
                        // execute the command
                        rdr2 = cmd2.ExecuteReader();
                        /*string querystr = "INSERT INTO Result([HomeTeamGoals]      ,[AwayTeamGoals]      ,[FixtureId])VALUES( @HomeTeamGoals, @AwayTeamGoals,@FixtureId)";
                        SqlCommand query = new SqlCommand(querystr, connection);

                        query.Parameters.Add("@HomeTeamGoals", SqlDbType.Int);
                        query.Parameters["@HomeTeamGoals"].Value = homeGoals;
                        query.Parameters.Add("@AwayTeamGoals", SqlDbType.Int);
                        query.Parameters["@AwayTeamGoals"].Value = awayGoals;
                        query.Parameters.Add("@FixtureId", SqlDbType.Int);
                        query.Parameters["@FixtureId"].Value = fixtureId;
                        query.ExecuteNonQuery();*/
                    }

                    command.Connection.Close();
                    return true;
                }
                catch (Exception ex)
                {

                    return false;
                }
            }
            return true;
        }
        /*static IHostBuilder CreateHostBuilder(string[] args) =>
       Host.CreateDefaultBuilder(args)
       .ConfigureServices((_, services) =>
       {
           services.AddTransient<Worker>();
       })
       .ConfigureLogging((_, logging) =>
       {
           logging.ClearProviders();
           logging.AddSimpleConsole(options => options.IncludeScopes = true);
           logging.AddEventLog();
       });*/
    }
}
